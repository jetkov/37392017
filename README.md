# FIRST Robotics Team 3739: Oakbotics

[![Competition Ready](https://img.shields.io/badge/competition%20ready-yes-green.svg)]() | 
[![Drive](https://img.shields.io/badge/drive-yes-green.svg)]()
[![Actuation](https://img.shields.io/badge/actuation-yes-green.svg)]() 
[![Gear](https://img.shields.io/badge/gear-yes-green.svg)]()
[![Autonomous](https://img.shields.io/badge/autonomous-wip-orange.svg)]() 
[![Vision](https://img.shields.io/badge/vision-wip-orange.svg)]()
[![Team Spirit](https://img.shields.io/badge/team%20spirit-86%-green.svg)]()

## About Us

Welcome to the Git repository for FIRST Robotics team 3739: Oakbotics.
Here you will find the code currently being written by the programming team 
for the FRC 2017 season! We are currently using Java.

## Repository Structure ⚠

DEVELOPERS, THIS IS IMPORTANT!

 - This repository is going to be structed in terms of features. 
 - For each feature that is added, a new branch of master will be created
 - Specifically to house very preliminary testing code is the "prototyping" branch
 - The prototyping branch will contain an Eclipse project for each feature that is being prototyped
 - The prototyping branch will never be merged with master

### Prototyping Branch 

This branch will be treated as a sort of sandbox to consolidate all features in preliminary testing, 
before those features get their own branches. The reason for this is so that 
explicity prototype code is seperated from feature code that is destined to be 
mereged with master. 