This repository is going to be structed in terms of features. For each feature 
that is added, a new branch of master will be created. Also, specifically to 
house very preliminary testing code, is the "testing" branch. This branch will 
contain an Eclipse project for each feature that is being tested in it, and 
this branch will never be merged with master. It will be treated as a sort of 
sandbox to consolidate all features in preliminary testing, before those 
features get their own branches. The reason for this is so that explicity 
prototype code is seperated from feature code that is destined to be mereged 
with master.