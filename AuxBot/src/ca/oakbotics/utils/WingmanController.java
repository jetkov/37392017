package ca.oakbotics.utils;

import edu.wpi.first.wpilibj.DriverStation;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.hal.HAL;
import edu.wpi.first.wpilibj.hal.FRCNetComm.tResourceType;

public class WingmanController extends GenericHID {
	private DriverStation m_ds;
	private short m_leftRumble;
	private short m_rightRumble;
	private int m_outputs;

	public WingmanController(final int port) {
		super(port);
		m_ds = DriverStation.getInstance();

		HAL.report(tResourceType.kResourceType_Joystick, port);
	}

	@Override
	public double getRawAxis(final int axis) {
		// TODO Auto-generated method stub
		return m_ds.getStickAxis(getPort(), axis);
	}

	public boolean getBumper(Hand hand) {
		// TODO Auto-generated method stub
		if (hand.equals(Hand.kLeft)) {
			return getRawButton(7);
		} else {
			return getRawButton(8);
		}
	}

	public boolean getRawButton(final int button) {
		// TODO Auto-generated method stub
		return m_ds.getStickButton(getPort(), (byte) button);
	}

	@Override
	public int getPOV(int pov) {
		return m_ds.getStickPOV(getPort(), pov);
	}

	@Override
	public int getPOVCount() {
		// TODO Auto-generated method stub
		return m_ds.getStickPOVCount(getPort());
	}

	@Override
	public HIDType getType() {
		// TODO Auto-generated method stub
		return HIDType.values()[m_ds.getJoystickType(getPort())];
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setOutput(int outputNumber, boolean value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setOutputs(int value) {

	}

	@Override
	public void setRumble(RumbleType type, double value) {
		if (value < 0) {
			value = 0;
		} else if (value > 1) {
			value = 1;
		}
		if (type == RumbleType.kLeftRumble) {
			m_leftRumble = (short) (value * 65535);
		} else {
			m_rightRumble = (short) (value * 65535);
		}
		HAL.setJoystickOutputs((byte) getPort(), m_outputs, m_leftRumble, m_rightRumble);
	}

	@Override
	public double getX(Hand hand) {
		if (hand.equals(Hand.kLeft)) {
			return getRawAxis(0);
		} else {
			return getRawAxis(3);
		}
	}

	@Override
	public double getY(Hand hand) {
		if (hand.equals(Hand.kLeft)) {
			return -getRawAxis(1);
		} else {
			return -getRawAxis(4);
		}
	}

	public double getSlider() {
		return -getRawAxis(2);
	}
}
