package ca.oakbotics.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import ca.oakbotics.robot.Config;
import ca.oakbotics.robot.commands.ControllerArcadeDrive;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.XboxController;

/**
 * The DriveTrain subsystem currently includes the two pairs of drive motors
 * (controllers).
 * 
 * @author Soran
 */
public class DriveTrain extends Subsystem {
	private Victor rMotors;
	private Victor lMotors;
	private RobotDrive drive;
	private ADXRS450_Gyro gyro;
	
	private AnalogInput ultrasonic;
	private static final double kUltraCm = 111.111; // cm/V
	private static final double kUltraMin = 30, kUltraMax = 75; // Min and max range for sensor
	public DriveTrain() {

		// Speed controller definitions.
		lMotors = new Victor(Config.L_DRIVE_MTRS_PORT);
		rMotors = new Victor(Config.R_DRIVE_MTRS_PORT);

		// Setting the motors to inverted (due to the gear boxes).
		lMotors.setInverted(Config.L_DRIVE_MTRS_INVRTD);
		rMotors.setInverted(Config.R_DRIVE_MTRS_INVRTD);

		// Defining the drive system.
		drive = new RobotDrive(lMotors, rMotors);
		
		gyro = new ADXRS450_Gyro();
		ultrasonic = new AnalogInput(0);

		// Adding the drive motor controllers to the Live Window.
		LiveWindow.addActuator("Drive Train", "Left Motors", lMotors);
		LiveWindow.addActuator("Drive Train", "Right Motors", rMotors);
	}

	public void initDefaultCommand() {
		setDefaultCommand(new ControllerArcadeDrive());
	}

	public void drive(XboxController controller) {
		double moveValue = controller.getTriggerAxis(GenericHID.Hand.kRight);
		double rotateValue = controller.getX(GenericHID.Hand.kLeft);
		drive.arcadeDrive(moveValue, rotateValue, true);
		
	}
	public void drive(double moveValue, double rotateValue){
		drive.arcadeDrive(-moveValue, rotateValue);
	}

	public void reset() {
		gyro.reset();
	}
	
	public double getAngle() {
		return gyro.getAngle();
	}
	
	public double getUltrasonicDistance() {
		double rawDistance = ultrasonic.getAverageVoltage() * kUltraCm;
		return Math.max(Math.min(rawDistance, kUltraMax), kUltraMin); // Limits the range in case of error
	}
}
