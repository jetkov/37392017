package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Robot;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Command;

public class TurnToDegree extends Command {
	private PIDController pid;
	private double kP = 0.05;
	private double kI = 0;
	private double kD = 0.1;

	public TurnToDegree(double degree) {

		requires(Robot.driveTrain);
		pid = new PIDController(kP, kI, kD, new PIDSource() {

			PIDSourceType m_sourceType = PIDSourceType.kDisplacement;

			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
				m_sourceType = pidSource;
			}

			@Override
			public PIDSourceType getPIDSourceType() {
				return m_sourceType;
			}

			@Override
			public double pidGet() {
				return Robot.driveTrain.getAngle();
			}
		}, new PIDOutput() {
			@Override
			public void pidWrite(double rotate) {
				Robot.driveTrain.drive(0, -rotate);
			}
		});
		pid.setAbsoluteTolerance(0.05);
		pid.setSetpoint(degree);
	}

	@Override
	protected void initialize() {
		// Get everything in a safe starting state.
		Robot.driveTrain.reset();
		pid.reset();
		pid.enable();
	}

	protected void end() {
		// Stop PID and the wheels
		pid.disable();
		Robot.driveTrain.drive(0, 0);
	}

	@Override
	protected boolean isFinished() {
		return false;
	}
}
