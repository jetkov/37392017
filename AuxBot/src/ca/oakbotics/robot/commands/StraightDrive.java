package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Robot;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Command;

public class StraightDrive extends Command {
	private PIDController gyroPID;
	private double kPG = 0.05;
	private double kIG = 0;
	private double kDG = 0.05;

	double curve = 0;
	
	public StraightDrive() {

		requires(Robot.driveTrain);
		gyroPID = new PIDController(kPG, kIG, kDG, new PIDSource() {

			PIDSourceType m_sourceType = PIDSourceType.kDisplacement;

			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
				m_sourceType = pidSource;
			}

			@Override
			public PIDSourceType getPIDSourceType() {
				return m_sourceType;
			}

			@Override
			public double pidGet() {
				return Robot.driveTrain.getAngle();
			}
		}, new PIDOutput() {
			@Override
			public void pidWrite(double pidCurve) {
				curve = pidCurve;
			}
		});
		gyroPID.setAbsoluteTolerance(0.05);
		gyroPID.setSetpoint(0);
	}

	@Override
	protected void initialize() {
		// Get everything in a safe starting state.
		Robot.driveTrain.reset();
		gyroPID.reset();
		gyroPID.enable();
	}

	// Called repeatedly when this Command is scheduled to run
	@Override
	protected void execute() {
		Robot.driveTrain.drive(0.5, -curve);
	}

	protected void end() {
		// Stop PID and the wheels
		gyroPID.disable();
		Robot.driveTrain.drive(0, 0);
	}

	@Override
	protected boolean isFinished() {
		return false;
	}
}
