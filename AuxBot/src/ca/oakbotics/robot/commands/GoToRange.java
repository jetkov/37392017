package ca.oakbotics.robot.commands;
import ca.oakbotics.robot.Robot;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
public class GoToRange extends Command {
	private PIDController ultraPID;
	private double kP = 0.02;
	private double kI = 0.00;
	private double kD = 0.05;
	public GoToRange(double cm) { // TODO Make this so values outside of min/max cant be entered (will cause robot to explode)
		requires(Robot.driveTrain);
		
		ultraPID = new PIDController(kP, kI, kD, new PIDSource() {
			PIDSourceType m_sourceType = PIDSourceType.kDisplacement;
			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
				m_sourceType = pidSource;
			}
			@Override
			public PIDSourceType getPIDSourceType() {
				return m_sourceType;
			}
			@Override
			public double pidGet() {
				double distance = Robot.driveTrain.getUltrasonicDistance();
				SmartDashboard.putNumber("Ultrasonic Range (cm)", distance);
				return Robot.driveTrain.getUltrasonicDistance();
			}
		}, new PIDOutput() {
			@Override
			public void pidWrite(double speed) {
				speed = Math.min(0.3, speed); // Speed limits
				Robot.driveTrain.drive(speed, 0);
			}
		});
		ultraPID.setAbsoluteTolerance(0.5);
		ultraPID.setSetpoint(cm);
	}
	@Override
	protected void initialize() {
		// Get everything in a safe starting state.
		ultraPID.reset();
		ultraPID.enable();
	}
	protected void end() {
		// Stop PID and the wheels
		ultraPID.disable();
		Robot.driveTrain.drive(0, 0);
	}
	@Override
	protected boolean isFinished() {
		return false;
	}
	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	@Override
	protected void interrupted() {
		// Stop PID and the wheels
		ultraPID.disable();
		Robot.driveTrain.drive(0, 0);
	}
}