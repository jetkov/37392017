package ca.oakbotics.robot.commands;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.command.Command;
import ca.oakbotics.robot.Config;
import ca.oakbotics.robot.Robot;

/**
 * This command allows the robot to be controlled by an Xbox controller. The
 * right trigger controls forward motion, the left trigger controls reverse,
 * and the left stick controls turning. If 'X' is held, the left and right
 * trigger controls or swapped (the right controls reverse).
 */
public class ControllerArcadeDrive extends Command {
	
	private XboxController controller = Robot.oi.getController();
	private double lTrig, rTrig, moveValue, rotateValue;
	
	public ControllerArcadeDrive() {
		// Use requires() here to declare subsystem dependencies.
		requires(Robot.driveTrain);
	}

	// Called just before this Command runs the first time.
	@Override
	protected void initialize() {
	}

	// Called repeatedly when this Command is scheduled to run.
	@Override
	protected void execute() {
		// Getting the Xbox controller trigger values.
		lTrig = controller.getTriggerAxis(GenericHID.Hand.kLeft);
		rTrig = controller.getTriggerAxis(GenericHID.Hand.kRight);
		
		// Subtracts the value of the left trigger from the value of the right one.
		// This effectively makes the right trigger forward control and left trigger reverse.
		moveValue = rTrig - lTrig;
		moveValue = moveValue * (Config.THROTTLE_PERCENTAGE/100);
		
		// Grabs the left stick 'x' value.
		rotateValue = controller.getX(GenericHID.Hand.kLeft);
		// Squares and multiplies this value by the set sensitivity value in 'Config'.
		// This makes turning easier to control.
		rotateValue = (rotateValue * Math.abs(rotateValue)) * (Config.ROTATE_PERCENTAGE/100);
		
		// If the 'X' button is pressed, the left and right trigger controls are effectively switched.
		if (controller.getXButton()) {
			Robot.driveTrain.drive(moveValue, rotateValue);
		}
		else Robot.driveTrain.drive(-moveValue, rotateValue);
		
	}
	@Override
	protected boolean isFinished() {
		return false;
	}

	// Called once after isFinished returns true
	@Override
	protected void end() {
		// Stops power to the drive train when this commend is finished
		Robot.driveTrain.drive(0, 0);
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	@Override
	protected void interrupted() {
	}
}
