package ca.oakbotics.robot;

import edu.wpi.first.wpilibj.XboxController;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {

	// Defining the Xbox controller
	private XboxController controller = new XboxController(0);

	public OI() {
	}

	// Publicly returns the Xbox controller object
	public XboxController getController() {
		return controller;
	}
}
