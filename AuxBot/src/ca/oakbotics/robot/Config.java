package ca.oakbotics.robot;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */

public class Config {
	
	public final static boolean USING_CONTROLLER = true;  	// "True" if using an xBox controller to drive the robot.
	public final static int CONTROLLER_PORT = 0;  			// Port of the xBox controller
	
	// Drive motors configuration
	public final static int L_DRIVE_MTRS_PORT = 0, R_DRIVE_MTRS_PORT = 1;		// The PWM ports that the left and right motor controllers are plugged in to
	public final static boolean L_DRIVE_MTRS_INVRTD = false, R_DRIVE_MTRS_INVRTD = false; 	// These motors are set as "inverted" to counteract how 
																				// the gear box by nature reverses the output from the motors

	// Drive sensitivity percentage modifiers (eg. controller rotate value multiplied by 70%)
	public final static double THROTTLE_PERCENTAGE = 75, ROTATE_PERCENTAGE = 75;  

}
