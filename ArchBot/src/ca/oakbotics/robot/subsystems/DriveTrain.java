package ca.oakbotics.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import ca.oakbotics.robot.Config;
import ca.oakbotics.robot.commands.ControllerArcadeDrive;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.XboxController;

/**
 * The DriveTrain subsystem currently includes the two pairs of drive motors (controllers).
 * 
 * @author Soran
 */
public class DriveTrain extends Subsystem {

	private Victor rMotors;
	private Victor lMotors;
	private RobotDrive drive;

	private Encoder encL, encR;
	private ADXRS450_Gyro gyro;
	private AnalogInput ultrasonic;
	
	private static final double kUltraCm = 111.111; // cm/V
	private static final double kUltraMin = 30, kUltraMax = 75; // Min and max
																// sensor range
	public DriveTrain() {

		// Speed controller definitions.
		lMotors = new Victor(Config.L_DRIVE_MTRS_PORT);
		rMotors = new Victor(Config.R_DRIVE_MTRS_PORT);

		// Setting the motors to inverted (due to the gear boxes).
		lMotors.setInverted(Config.L_DRIVE_MTRS_INVRTD);
		rMotors.setInverted(Config.R_DRIVE_MTRS_INVRTD);

		// Defining the drive system.
		drive = new RobotDrive(lMotors, rMotors);

		// Defining the sensors
		encL = new Encoder(0, 1, false);
		encR = new Encoder(2, 3, false);
		gyro = new ADXRS450_Gyro();
		ultrasonic = new AnalogInput(Config.ULTRASONIC_SENS_PORT);

		// Configuring the encoders
		encL.setMaxPeriod(0.1);
		encL.setMinRate(0.5);
		encL.setDistancePerPulse(0.2233630606); // Centimeters
		encL.setReverseDirection(false);
		encL.setSamplesToAverage(7);
		
		encR.setMaxPeriod(0.1);
		encR.setMinRate(0.5);
		encR.setDistancePerPulse(0.2310568718); // Centimeters
		encR.setReverseDirection(true);
		encR.setSamplesToAverage(7);

		// Adding the drive motor controllers to the Live Window.
		LiveWindow.addActuator("Drive Train", "Left Motors", lMotors);
		LiveWindow.addActuator("Drive Train", "Right Motors", rMotors);
		
		LiveWindow.addActuator("Encoder", "Right", encR);
		LiveWindow.addActuator("Encoder", "Left", encL);
		LiveWindow.addSensor("Gyro", "Angle", gyro);
	}

	public void initDefaultCommand() {
		setDefaultCommand(new ControllerArcadeDrive());
	}

	
	public void arcadeDrive(XboxController controller) {
		double moveValue = controller.getTriggerAxis(GenericHID.Hand.kRight);
		double rotateValue = controller.getX(GenericHID.Hand.kLeft);
		drive.arcadeDrive(moveValue, rotateValue, true);

	}
	
	public void arcadeDrive(double moveValue, double rotateValue){
		drive.arcadeDrive(-moveValue, rotateValue, false);
	}
	
	public void arcadeDrive(double moveValue, double rotateValue, boolean squaredInputs){
		drive.arcadeDrive(moveValue, rotateValue, squaredInputs);
	}

	public void manualDrive(double moveValue, double rotateValue) {
		// For some reason RobotDrive.drive() has an inverted move value
		drive.drive(-moveValue, rotateValue);
	}

	public void resetEncoders() {
		encL.reset();
		encR.reset();
	}
	
	public double getAvgEncoderDistance() {
		return (encL.getDistance() + encR.getDistance()) / 2;
	}
	
	public void reset() {
		resetGyro();
		resetEncoders();
		manualDrive(0, 0);
	}
	
	public void resetGyro() {
//		try {
			gyro.reset();
//		}
//		catch (Exception e)
//		{
//			System.out.println("WARNING: Gyro not detected or functioning properly!");
//		}
	}

	public double getAngle() {
		return gyro.getAngle();
//		try {
//			return gyro.getAngle();
//		}
//		catch (Exception e)
//		{
//			System.out.println("WARNING: Gyro not detected or functioning properly!");
//			return 0;
//		}
	}

	public double getUltrasonicDistance() {
		double rawDistance = ultrasonic.getAverageVoltage() * kUltraCm;
		return Math.max(Math.min(rawDistance, kUltraMax), kUltraMin); // Limits the range in case of error
	}
}
