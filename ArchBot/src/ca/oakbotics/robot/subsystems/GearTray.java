package ca.oakbotics.robot.subsystems;

import ca.oakbotics.robot.Config;
import ca.oakbotics.robot.commands.GearTrayClose;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
/**
 *
 */
public class GearTray extends Subsystem {

	private DoubleSolenoid gearDS;

	public GearTray() {
		gearDS = new DoubleSolenoid(Config.GEAR_DS_A, Config.GEAR_DS_B);
		LiveWindow.addActuator("Pneumatics", "Gear Piston", gearDS);
	}
	public void initDefaultCommand() {
		setDefaultCommand(new GearTrayClose());
	}

	public void forward() {
		gearDS.set(DoubleSolenoid.Value.kForward);
	}

	public void reverse() {
		gearDS.set(DoubleSolenoid.Value.kReverse);
	}
	public void off() {
		gearDS.set(DoubleSolenoid.Value.kOff);
	}
}