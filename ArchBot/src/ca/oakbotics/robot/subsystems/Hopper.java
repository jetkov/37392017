package ca.oakbotics.robot.subsystems;

import ca.oakbotics.robot.Config;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;

/**
 *
 */
public class Hopper extends Subsystem {
	private DoubleSolenoid hopperDS;

	public Hopper() {

		hopperDS = new DoubleSolenoid(Config.HOPPER_DS_PORT_A, Config.HOPPER_DS_PORT_B);

		LiveWindow.addActuator("Hopper", "Double Solenoid", hopperDS);
	}

	public void initDefaultCommand() {
	}


	public void dumpBalls() {
		hopperDS.set(DoubleSolenoid.Value.kForward);
	}
	
	public void resetHopper() {
		hopperDS.set(DoubleSolenoid.Value.kReverse);
	}
	
	public void hopperSolenoidOff() {
		hopperDS.set(DoubleSolenoid.Value.kOff);
	}
}
