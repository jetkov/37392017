package ca.oakbotics.robot.subsystems;
import ca.oakbotics.robot.Config;
import ca.oakbotics.robot.commands.WinchIn;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
public class Winch extends Subsystem {
	private Victor motor1;
	private Victor motor2;
	public Winch() {
		
		motor1 = new Victor(Config.WINCH_MTR1_PORT);
		motor1.setInverted(Config.WINCH_MTR1_INVRTD);
		
		motor2 = new Victor(Config.WINCH_MTR2_PORT);
		motor2.setInverted(Config.WINCH_MTR2_INVRTD);
		
		LiveWindow.addActuator("Winch", "Motor 1", motor1);
		LiveWindow.addActuator("Winch", "Motor 2", motor2);
	}
	@Override
	public void initDefaultCommand() {
		setDefaultCommand(new WinchIn());
	}
	public void setSpeed(double speed) {
		speed = Math.max(0, speed);
		motor1.set(speed);
		motor2.set(speed);
	}
	public void stopWinch() {
		motor1.set(0);
		motor2.set(0);
	}

}