package ca.oakbotics.robot.subsystems;

import ca.oakbotics.robot.Config;
import ca.oakbotics.robot.commands.BallIntakeIn;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;

/**
 *
 */
public class BallIntake extends Subsystem {
	private Victor motor;

	public BallIntake() {
		motor = new Victor(Config.INTAKE_MTR_PORT);
		motor.setInverted(Config.INTAKE_MTR_INVRTD);

		LiveWindow.addActuator("Ball Intake", "Motor", motor);
	}

	public void initDefaultCommand() {
		setDefaultCommand(new BallIntakeIn());
	}

	public void setIntakeSpeed(double speed) {
		motor.set(speed);
	}

	public void stopIntake() {
		motor.set(0);
	}

}
