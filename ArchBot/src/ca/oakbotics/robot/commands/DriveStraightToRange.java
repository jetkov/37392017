package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Config;
import ca.oakbotics.robot.Robot;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class DriveStraightToRange extends Command {
	private PIDController ultraPID, gyroPID;

	private double kPG = 0.05, kIG = 0, kDG = 0.05; // Gyro PID
	private double kPU = 0.02, kIU = 0.00, kDU = 0.05; // Ultrasonic PID

	private double speed, curve;

	public DriveStraightToRange(double cm) { 
		requires(Robot.driveTrain);

		ultraPID = new PIDController(kPU, kIU, kDU, new PIDSource() {
			PIDSourceType m_sourceType = PIDSourceType.kDisplacement;

			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
				m_sourceType = pidSource;
			}

			@Override
			public PIDSourceType getPIDSourceType() {
				return m_sourceType;
			}

			@Override
			public double pidGet() {
				double distance = Robot.driveTrain.getUltrasonicDistance();
				SmartDashboard.putNumber("Ultrasonic Range (cm)", distance);
				return Robot.driveTrain.getUltrasonicDistance();
			}
		}, new PIDOutput() {
			@Override
			public void pidWrite(double pidSpeed) {
				speed = pidSpeed;
			}
		});
		ultraPID.setAbsoluteTolerance(0.5);
		ultraPID.setSetpoint(cm);
		
		gyroPID = new PIDController(kPG, kIG, kDG, new PIDSource() {

			PIDSourceType m_sourceType = PIDSourceType.kDisplacement;

			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
				m_sourceType = pidSource;
			}

			@Override
			public PIDSourceType getPIDSourceType() {
				return m_sourceType;
			}

			@Override
			public double pidGet() {
				return Robot.driveTrain.getAngle();
			}
		}, new PIDOutput() {
			@Override
			public void pidWrite(double pidCurve) {
				curve = pidCurve;
			}
		});
		gyroPID.setAbsoluteTolerance(0.05);
		gyroPID.setSetpoint(0);

		
	}

	@Override
	protected void initialize() {
		// Get everything in a safe starting state.
		gyroPID.reset();
		gyroPID.enable();

		ultraPID.reset();
		ultraPID.enable();
	}

	// Called repeatedly when this Command is scheduled to run
	@Override
	protected void execute() {
		speed = Math.max(Math.min(Config.AUTO_SPEED_LIMIT, speed), -Config.AUTO_SPEED_LIMIT);
		Robot.driveTrain.manualDrive(speed, curve);
	}

	protected void end() {
		// Stop PID and the wheels
		ultraPID.disable();
		gyroPID.disable();
		Robot.driveTrain.manualDrive(0, 0);
	}

	@Override
	protected boolean isFinished() {
		return ultraPID.onTarget();
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	@Override
	protected void interrupted() {
		// Stop PID and the wheels
		ultraPID.disable();
		gyroPID.disable();
		Robot.driveTrain.manualDrive(0, 0);
	}
}