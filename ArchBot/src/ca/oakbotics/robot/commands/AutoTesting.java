package ca.oakbotics.robot.commands;

import edu.wpi.first.wpilibj.command.CommandGroup;

/**
 * The main autonomous command to pickup and deliver the soda to the box.
 */
public class AutoTesting extends CommandGroup {
	public AutoTesting() {
		addSequential(new DriveStraightToDistance(180), 3);
		addSequential(new GearTrayOpen());
		addSequential(new DriveStraightToDistance(-50), 3);
	}
}
