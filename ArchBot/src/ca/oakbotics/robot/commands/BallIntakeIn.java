package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Robot;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 * Sets the intake speed to the wingman slider
 *
 * @author Alex
 *
 */
public class BallIntakeIn extends Command {
	
	// WingmanController wingman = Robot.oi.getWingman(); Can't create another instance!?!?!

	public BallIntakeIn() {
		// Use requires() here to declare subsystem dependencies
		requires(Robot.ballIntake);
	}

	// Called just before this Command runs the first time
	@Override
	protected void initialize() {
	}

	// Called repeatedly when this Command is scheduled to run
	@Override
	protected void execute() {
		double speed = Robot.oi.getXboxOp().getTriggerAxis(Hand.kRight);
		SmartDashboard.putNumber("BI Speed", speed);
		Robot.ballIntake.setIntakeSpeed(speed);
	}

	// Make this return true when this Command no longer needs to run execute()
	@Override
	protected boolean isFinished() {
		return false;
	}

	// Called once after isFinished returns true
	@Override
	protected void end() {
		Robot.ballIntake.setIntakeSpeed(0);
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	@Override
	protected void interrupted() {
		Robot.ballIntake.stopIntake();
	}
}
