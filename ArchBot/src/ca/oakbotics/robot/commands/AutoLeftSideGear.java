package ca.oakbotics.robot.commands;

import edu.wpi.first.wpilibj.command.CommandGroup;

/**
 * The main autonomous command to pickup and deliver the soda to the box.
 */
public class AutoLeftSideGear extends CommandGroup {
	public AutoLeftSideGear() {
		addSequential(new DriveStraightToDistance(220), 3);
		addSequential(new TurnToDegree(60), 3);
		addSequential(new DriveStraightToDistance(25), 3);
//		addSequential(new DriveStraightToRange(30), 0.5);
//		addSequential(new GearTrayOpen());
	}
}
