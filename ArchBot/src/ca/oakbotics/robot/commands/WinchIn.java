package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Robot;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class WinchIn extends Command {

	private double speed;
	
	public WinchIn() {
		requires(Robot.winch);
		speed = 0;
	}
	
	public WinchIn(double speed) {
		requires(Robot.winch);
		this.speed = speed;
	}

	// Called just before this Command runs the first time
	@Override
	protected void initialize() {
	}

	@Override
	protected void execute() {
		speed = Robot.oi.getXboxOp().getTriggerAxis(Hand.kLeft);
		speed *= speed;
		SmartDashboard.putNumber("WincH Speed", speed);
		Robot.winch.setSpeed(speed);
	}

	@Override
	protected boolean isFinished() {
		return false;
	}

	@Override
	protected void end() {
		Robot.winch.setSpeed(0);
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	@Override
	protected void interrupted() {
		Robot.winch.setSpeed(0);
	}
}
