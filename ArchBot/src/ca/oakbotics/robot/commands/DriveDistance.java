package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Config;
import ca.oakbotics.robot.Robot;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 */
public class DriveDistance extends Command {

	private PIDController encoderPID;

	private final double kP = 0.025, kI = 0, kD = 0.105;

	double speed;

	public DriveDistance(double cm) {
		// Use requires() here to declare subsystem dependencies
		requires(Robot.driveTrain);
		encoderPID = new PIDController(kP, kI, kD, new PIDSource() {
			PIDSourceType m_sourceType = PIDSourceType.kDisplacement;

			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
				m_sourceType = pidSource;
			}

			@Override
			public PIDSourceType getPIDSourceType() {
				return m_sourceType;
			}

			@Override
			public double pidGet() {
				double distance = Robot.driveTrain.getAvgEncoderDistance();
				SmartDashboard.putNumber("Avg Encoder Distance", distance);
				return distance;
			}
		}, new PIDOutput() {
			@Override
			public void pidWrite(double pidSpeed) {
				speed = pidSpeed;
			}
		});
		encoderPID.setAbsoluteTolerance(0.05);
		encoderPID.setSetpoint(cm);
	}

	// Called just before this Command runs the first time
	@Override
	protected void initialize() {
		// Get everything in a safe starting state.
		Robot.driveTrain.resetEncoders();
		encoderPID.reset();
		encoderPID.enable();
	}

	// Called repeatedly when this Command is scheduled to run
	@Override
	protected void execute() {
		speed = Math.min(Math.max(speed, -Config.AUTO_SPEED_LIMIT), Config.AUTO_SPEED_LIMIT);
		Robot.driveTrain.manualDrive(speed, 0);
	}

	// Make this return true when this Command no longer needs to run execute()
	@Override
	protected boolean isFinished() {
		return encoderPID.onTarget();
	}

	// Called once after isFinished returns true
	@Override
	protected void end() {
		encoderPID.disable();
		Robot.driveTrain.manualDrive(0, 0);
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	@Override
	protected void interrupted() {
		encoderPID.disable();
		Robot.driveTrain.manualDrive(0, 0);
	}
}