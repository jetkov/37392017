package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Robot;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Command;

public class DriveStraight extends Command {
	private PIDController gyroPID;
	private double kP = 0.05;
	private double kI = 0;
	private double kD = 0.05;

	public DriveStraight(double speed) {

		requires(Robot.driveTrain);
		gyroPID = new PIDController(kP, kI, kD, new PIDSource() {

			PIDSourceType m_sourceType = PIDSourceType.kDisplacement;

			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
				m_sourceType = pidSource;
			}

			@Override
			public PIDSourceType getPIDSourceType() {
				return m_sourceType;
			}

			@Override
			public double pidGet() {
				return Robot.driveTrain.getAngle();
			}
		}, new PIDOutput() {
			@Override
			public void pidWrite(double rotate) {
				Robot.driveTrain.manualDrive(speed, -rotate);
			}
		});
		gyroPID.setAbsoluteTolerance(0.05);
		gyroPID.setSetpoint(0);
	}

	@Override
	protected void initialize() {
		// Get everything in a safe starting state.
		Robot.driveTrain.reset();
		gyroPID.reset();
		gyroPID.enable();
	}

	protected void end() {
		// Stop PID and the wheels
		gyroPID.disable();
		Robot.driveTrain.manualDrive(0, 0);
	}

	@Override
	protected boolean isFinished() {
		return false;
	}
}
