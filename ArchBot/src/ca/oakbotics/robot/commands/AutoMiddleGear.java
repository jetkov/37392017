package ca.oakbotics.robot.commands;

import edu.wpi.first.wpilibj.command.CommandGroup;

/**
 * The main autonomous command to pickup and deliver the soda to the box.
 */
public class AutoMiddleGear extends CommandGroup {
	public AutoMiddleGear() {
		addSequential(new DriveStraightToDistance(195), 3);
//		addSequential(new DriveStraightToRange(40));
		addSequential(new GearTrayOpen(), 2);
		addSequential(new DriveStraightToDistance(-30), 3);
	}
}
