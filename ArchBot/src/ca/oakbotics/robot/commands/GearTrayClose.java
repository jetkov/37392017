package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Robot;
import edu.wpi.first.wpilibj.command.Command;

public class GearTrayClose extends Command {
	public GearTrayClose() {
		requires(Robot.gearTray);
	}

	@Override
	protected void initialize() {
		Robot.gearTray.reverse();
	}
	
	@Override
	protected void end() {
		Robot.gearTray.off();
	}

	@Override
	protected boolean isFinished() {
		return false;
	}
}

