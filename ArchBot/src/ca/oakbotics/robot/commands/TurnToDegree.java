package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Robot;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class TurnToDegree extends Command {
	
	private PIDController gyroPID;
	private double kP = 0.05, kI = 0, kD = 0.1;

	private double rotate;

	public TurnToDegree(double degree) {

		requires(Robot.driveTrain);
		gyroPID = new PIDController(kP, kI, kD, new PIDSource() {

			PIDSourceType m_sourceType = PIDSourceType.kDisplacement;

			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
				m_sourceType = pidSource;
			}

			@Override
			public PIDSourceType getPIDSourceType() {
				return m_sourceType;
			}

			@Override
			public double pidGet() {
				double angle = Robot.driveTrain.getAngle();
				SmartDashboard.putNumber("Gyro Angle", angle);
				return angle;
			}
		}, new PIDOutput() {
			@Override
			public void pidWrite(double pidRotate) {
				rotate = pidRotate;
			}
		});
		gyroPID.setAbsoluteTolerance(0.5);
		gyroPID.setSetpoint(degree);

		LiveWindow.addSensor("PID", "Gyro", gyroPID);
	}

	@Override
	protected void initialize() {
		// Get everything in a safe starting state.
		Robot.driveTrain.reset();
		gyroPID.reset();
		gyroPID.enable();
	}

	@Override
	protected void execute() {
		Robot.driveTrain.arcadeDrive(0, rotate);
	}
	
	@Override
	protected boolean isFinished() {
		return gyroPID.onTarget();
	}

	protected void end() {
		// Stop PID and the wheels
		gyroPID.disable();
		Robot.driveTrain.reset();
	}

	@Override
	protected void interrupted() {
		// Stop PID and the wheels
		gyroPID.disable();
		Robot.driveTrain.reset();
	}

}
