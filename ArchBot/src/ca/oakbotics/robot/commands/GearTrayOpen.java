package ca.oakbotics.robot.commands;

import edu.wpi.first.wpilibj.command.Command;

import ca.oakbotics.robot.Robot;

/**
 *
 */
public class GearTrayOpen extends Command {
	public GearTrayOpen() {
		requires(Robot.gearTray);
	}

	@Override
	protected void initialize() {
		Robot.gearTray.forward();
	}

	@Override
	protected void end() {
		Robot.gearTray.off();
	}

	@Override
	protected boolean isFinished() {
		return false;
	}
}
