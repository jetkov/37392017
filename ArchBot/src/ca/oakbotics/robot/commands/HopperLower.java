package ca.oakbotics.robot.commands;

import edu.wpi.first.wpilibj.command.Command;

import ca.oakbotics.robot.Robot;

/**
 *
 */
public class HopperLower extends Command {
	public HopperLower() {
		// Use requires() here to declare subsystem dependencies
		requires(Robot.hopper);
	}

	// Called just before this Command runs the first time
	@Override
	protected void initialize() {
		Robot.hopper.resetHopper();
	}

	// Make this return true when this Command no longer needs to run execute()
	@Override
	protected boolean isFinished() {
		return false;
	}

	// Called once after isFinished returns true
	@Override
	protected void end() {
		Robot.hopper.hopperSolenoidOff();
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	@Override
	protected void interrupted() {
		Robot.hopper.hopperSolenoidOff();
	}
}
