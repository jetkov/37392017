package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Config;
import ca.oakbotics.robot.Robot;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class DriveStraightToDistance extends Command {
	private PIDController encoderPID, gyroPID;

	private double kPG = 0.05, kIG = 0, kDG = 0.05; // Gyro PID
	private double kPE = 0.025, kIE = 0.00, kDE = 0.17; // Encoder PID

	private double speed, rotate;

	public DriveStraightToDistance(double cm) {
		requires(Robot.driveTrain);

		encoderPID = new PIDController(kPE, kIE, kDE, new PIDSource() {
			PIDSourceType m_sourceType = PIDSourceType.kDisplacement;

			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
				m_sourceType = pidSource;
			}

			@Override
			public PIDSourceType getPIDSourceType() {
				return m_sourceType;
			}

			@Override
			public double pidGet() {
				double distance = Robot.driveTrain.getAvgEncoderDistance();
				SmartDashboard.putNumber("Avg Encoder Distance", distance);
				return distance;
			}
		}, new PIDOutput() {
			@Override
			public void pidWrite(double pidSpeed) {
				speed = pidSpeed;
			}
		});
		encoderPID.setAbsoluteTolerance(0.5);
		encoderPID.setSetpoint(cm);
		//LiveWindow.addSensor("PID", "Encoder", encoderPID);

		gyroPID = new PIDController(kPG, kIG, kDG, new PIDSource() {

			PIDSourceType m_sourceType = PIDSourceType.kDisplacement;

			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
				m_sourceType = pidSource;
			}

			@Override
			public PIDSourceType getPIDSourceType() {
				return m_sourceType;
			}

			@Override
			public double pidGet() {
				return Robot.driveTrain.getAngle();
			}
		}, new PIDOutput() {
			@Override
			public void pidWrite(double pidRotate) {
				rotate = pidRotate;
			}
		});
		gyroPID.setAbsoluteTolerance(0.05);
		gyroPID.setSetpoint(0);

	}

	@Override
	protected void initialize() {
		// Get everything in a safe starting state.
		Robot.driveTrain.reset();

		gyroPID.reset();
		gyroPID.enable();

		encoderPID.reset();
		encoderPID.enable();
	}

	// Called repeatedly when this Command is scheduled to run
	@Override
	protected void execute() {
		speed = Math.max(Math.min(Config.AUTO_SPEED_LIMIT, speed), -Config.AUTO_SPEED_LIMIT);
		Robot.driveTrain.arcadeDrive(speed, rotate);
	}

	protected void end() {
		// Stop PID and the wheels
		encoderPID.disable();
		gyroPID.disable();
		Robot.driveTrain.reset();
	}

	@Override
	protected boolean isFinished() {
		return encoderPID.onTarget();
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	@Override
	protected void interrupted() {
		// Stop PID and the wheels
		encoderPID.disable();
		gyroPID.disable();
		Robot.driveTrain.reset();
	}
}