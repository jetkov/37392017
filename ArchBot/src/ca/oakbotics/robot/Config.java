package ca.oakbotics.robot;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */

public class Config {

	// USB ports
	public final static int DRIVE_XBOX_PORT = 0;   	// Driver Xbox controller USB port
	public final static int OP_XBOX_PORT = 1;		// Operator Xbox controller USB port
	
	// Drive sensitivity percentage modifiers (eg. controller rotate value multiplied by 70%)
	public final static double THROTTLE_PERCENTAGE = 70, ROTATE_PERCENTAGE = 70;
	
	// PWM ports
	public final static int L_DRIVE_MTRS_PORT = 1, R_DRIVE_MTRS_PORT = 0;	// These motors are set as "inverted" to counteract how
	public final static boolean L_DRIVE_MTRS_INVRTD = true, R_DRIVE_MTRS_INVRTD = true; 	// the gear box by nature reverses the output from the motors
	public final static int INTAKE_MTR_PORT = 3;
	public final static boolean INTAKE_MTR_INVRTD = true;	
										
	// Solenoid ports (DS = double-solenoid)
	public static final int GEAR_DS_A = 0, GEAR_DS_B = 1;
	public final static int HOPPER_DS_PORT_A = 2, HOPPER_DS_PORT_B = 3;
	
	// Sensor ports
	public static final int ULTRASONIC_SENS_PORT = 0;
	
	// Auto constants
	public static final double AUTO_SPEED_LIMIT = 0.4;	
	
	// Winch
	public final static int WINCH_MTR1_PORT = 5, WINCH_MTR2_PORT = 6;
	public final static boolean WINCH_MTR1_INVRTD = false, WINCH_MTR2_INVRTD = false;
	public static final int WINCH_DS_PORT_A = 4, WINCH_DS_PORT_B = 5;

}
