package ca.oakbotics.robot;

import ca.oakbotics.robot.subsystems.BallIntake;
import ca.oakbotics.robot.subsystems.ExampleSubsystem;
import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import ca.oakbotics.robot.OI;
import ca.oakbotics.robot.commands.AutoLeftSideGear;
import ca.oakbotics.robot.commands.AutoMiddleGear;
import ca.oakbotics.robot.commands.AutoRightSideGear;
import ca.oakbotics.robot.commands.AutoTesting;
import ca.oakbotics.robot.subsystems.DriveTrain;
import ca.oakbotics.robot.subsystems.GearTray;
import ca.oakbotics.robot.subsystems.Hopper;
import ca.oakbotics.robot.subsystems.Winch;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends IterativeRobot {

	public static final ExampleSubsystem exampleSubsystem = new ExampleSubsystem();
	public static final BallIntake ballIntake = new BallIntake();
	public static final Hopper hopper = new Hopper();
	public static final DriveTrain driveTrain = new DriveTrain();
	public static final GearTray gearTray = new GearTray();
	public static final Winch winch = new Winch();
	
	public static OI oi;
	
	Command autonomousCommand;
	SendableChooser<Command> chooser = new SendableChooser<>();

	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	@Override
	public void robotInit() {
		oi = new OI();
		
		// Adding the auton modes to the chooser
		chooser.addObject("Left Side Gear Auto", new AutoLeftSideGear());
		chooser.addDefault("Middle Gear Auto", new AutoMiddleGear());
		chooser.addObject("Right Side Gear Auto", new AutoRightSideGear());
		chooser.addObject("Auto Testing", new AutoTesting());
		
		SmartDashboard.putData("Auto mode", chooser);
		
		CameraServer.getInstance().startAutomaticCapture();
		
		driveTrain.reset();
	}

	/**
	 * This function is called once each time the robot enters Disabled mode.
	 * You can use it to reset any subsystem information you want to clear when
	 * the robot is disabled.
	 */
	@Override
	public void disabledInit() {
		driveTrain.reset();
	}

	@Override
	public void disabledPeriodic() {
		Scheduler.getInstance().run();
	}

	/**
	 * This autonomous (along with the chooser code above) shows how to select
	 * between different autonomous modes using the dashboard. The sendable
	 * chooser code works with the Java SmartDashboard. If you prefer the
	 * LabVIEW Dashboard, remove all of the chooser code and uncomment the
	 * getString code to get the auto name from the text box below the Gyro
	 *
	 * You can add additional auto modes by adding additional commands to the
	 * chooser code above (like the commented example) or additional comparisons
	 * to the switch structure below with additional strings & commands.
	 */
	@Override
	public void autonomousInit() {
		driveTrain.reset();
		
		autonomousCommand = chooser.getSelected();

		/*
		 * String autoSelected = SmartDashboard.getString("Auto Selector",
		 * "Default"); switch(autoSelected) { case "My Auto": autonomousCommand
		 * = new MyAutoCommand(); break; case "Default Auto": default:
		 * autonomousCommand = new ExampleCommand(); break; }
		 */

		// schedule the autonomous command (example)
		if (autonomousCommand != null)
			autonomousCommand.start();
	}

	/**
	 * This function is called periodically during autonomous
	 */
	@Override
	public void autonomousPeriodic() {
		Scheduler.getInstance().run();
	}

	@Override
	public void teleopInit() {
		// This makes sure that the autonomous stops running when
		// teleop starts running. If you want the autonomous to
		// continue until interrupted by another command, remove
		// this line or comment it out.
		if (autonomousCommand != null)
			autonomousCommand.cancel();

	}

	/**
	 * This function is called periodically during operator control
	 */
	@Override
	public void teleopPeriodic() {
		Scheduler.getInstance().run();
	}

	/**
	 * This function is called periodically during test mode
	 */
	@Override
	public void testPeriodic() {
		LiveWindow.run();
	}
}
