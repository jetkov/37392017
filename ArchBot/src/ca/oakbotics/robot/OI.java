package ca.oakbotics.robot;

import ca.oakbotics.robot.commands.HopperDump;
import ca.oakbotics.robot.commands.HopperLower;
import ca.oakbotics.robot.commands.WinchIn;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import ca.oakbotics.robot.commands.GearTrayClose;
import ca.oakbotics.robot.commands.GearTrayOpen;
import edu.wpi.first.wpilibj.XboxController;

/**
 * This class is the glue that binds the controls on the physical operator interface to the commands
 * and command groups that allow control of the robot.
 */
public class OI {

	private XboxController xboxDrive = new XboxController(Config.DRIVE_XBOX_PORT);
	private XboxController xboxOp = new XboxController(Config.OP_XBOX_PORT);

	public OI() {

		// SmartDashboard Buttons
		SmartDashboard.putData("Open Gear Tray", new GearTrayOpen());
		SmartDashboard.putData("Close Gear Tray", new GearTrayClose());

		// Mapping buttons
		JoystickButton a = new JoystickButton(xboxOp, 1);
		JoystickButton b = new JoystickButton(xboxOp, 2);
//		JoystickButton x = new JoystickButton(xboxOp, 3);
//		JoystickButton y = new JoystickButton(xboxOp, 4);

		JoystickButton lBump = new JoystickButton(xboxOp, 5);

		// Button actions
		a.whileHeld(new GearTrayOpen());
		b.whenPressed(new HopperDump());
		b.whenReleased(new HopperLower());

		// Toggle winch control with left joystick
		lBump.whileHeld(new WinchIn());
	}

	// Publicly returns the Xbox controller object
	public XboxController getXboxDrive() {
		return xboxDrive;
	}

	// Publicly returns the xboxOp controller object (this has been know to cause 
	// issues for some reason)
	public XboxController getXboxOp() {
		return xboxOp;
	}
}
